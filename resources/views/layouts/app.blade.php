<html>

<head>
    <title>Reservation Validator</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css"/>

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/locale/nl.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <style type="text/css">
        /* .multiselect-container {
            width: 100% !important;
        } */
    </style>

</head>

<body>
    @section('sidebar')

    @show

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="/settings">Reservation Validator</a>
            </div>
            <ul class="nav navbar-nav">
            <li><a href="/settings">Settings</a></li>
            <li><a href="/reservations">Reservations</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>
</body>

</html>
<script>
    $(document).ready(function() {
        $('#multiple-select').multiselect({
            includeSelectAllOption: true,
            buttonWidth: '100%'
        });
        var defaults = {
            calendarWeeks: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            useCurrent: false,
            ignoreReadonly: true,
            toolbarPlacement: 'top',
            locale: 'nl'
        };

        $(function() {
            var optionsDatetime = $.extend({}, defaults, {format:'DD-MM-YYYY HH:mm:ss'});
            $('.datetimepicker').datetimepicker(optionsDatetime);
        });
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $('.time_zone').val(timezone);
    });
    $("#reservation").on("submit", function(e){
        $.ajax({
          url: '/reservations',
          type: 'post',
          data: $('#reservation').serialize(),
          dataType: 'json',
          success: function(data) {
              alert(JSON.stringify(data));
          }
         });
         e.preventDefault();
    });
</script>
