@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Validate Reservation</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('settings.index') }}" title="Go back"> <i
                        class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form id="reservation" method="POST">
        @csrf
        @method('POST')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>user ids:</strong>
                    <select id="multiple-select" class="form-control" name="user_ids[]" multiple="multiple">
                        @foreach ($user_ids as $user_id)
                            <option value="{{$user_id}}">{{$user_id}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Reservation datetime:</strong>
                    <div class="input-group datetimepicker">
                        <input type="text" class="form-control" name="reservation_datetime[]" placeholder="DD-MM-YYYY HH:mm:ss" readonly>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                            <span class="fa fa-clock"></span>
                        </span>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control time_zone" name="time_zone">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
