@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Result</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('reservations.index') }}" title="Go back"> <i
                        class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>
    {{dd($data)}}
    @if ($data['is_booking_restricted'])
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some users restricted.<br><br>
            <ul>
                @foreach ($data['restricted_user_ids']as $user_id)
                    <li>{{ $user_id }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!$data['is_booking_restricted'])
        <div class="alert alert-success">
            <strong>Successfully Submitted.</strong>.
        </div>
    @endif
@endsection
