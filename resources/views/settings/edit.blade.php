@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Setting</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('settings.index') }}" title="Go back"> <i
                        class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('settings.update', $setting->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>n:</strong>
                    <input type="text" name="n" value="{{ $setting->n }}" class="form-control" placeholder="n">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>d:</strong>
                    <select class="form-control" name="d">
                        <option value="day"
                        @if ($setting->d == 'day')
                            selected="selected"
                        @endif
                        >day</option>
                        <option value="week"
                        @if ($setting->d == 'week')
                            selected="selected"
                        @endif
                        >week</option>
                        <option value="month"
                        @if ($setting->d == 'month')
                            selected="selected"
                        @endif
                        >month</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>g:</strong>
                    <select class="form-control" name="g">
                        <option value="individual"
                        @if ($setting->g == 'individual')
                            selected="selected"
                        @endif
                        >individual</option>
                        <option value="group"
                        @if ($setting->g == 'group')
                            selected="selected"
                        @endif
                        >group</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>tz:</strong>
                    <select class="form-control" name="tz">
                        <option value="UTC"
                        @if ($setting->tz == 'UTC')
                            selected="selected"
                        @endif
                        >UTC</option>
                        <option value="Asia/Calcutta"
                        @if ($setting->tz == 'Asia/Calcutta')
                            selected="selected"
                        @endif
                        >Asia/Calcutta</option>
                        <option value="America/NewYork"
                        @if ($setting->tz == 'America/NewYork')
                            selected="selected"
                        @endif
                        >America/NewYork</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
