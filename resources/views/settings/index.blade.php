@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Settings</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('settings.create') }}" title="Create a category"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>n</th>
            <th>d</th>
            <th>g</th>
            <th>tz</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($settings as $setting)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $setting->n }}</td>
                <td>{{ $setting->d }}</td>
                <td>{{ $setting->g }}</td>
                <td>{{ $setting->tz }}</td>
                <td>
                    <a href="{{ route('settings.edit', $setting->id) }}">
                        <i class="fas fa-edit  fa-lg"></i>

                    </a>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $settings->links() !!}

@endsection
