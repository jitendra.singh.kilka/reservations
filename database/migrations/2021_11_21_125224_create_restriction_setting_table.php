<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestrictionSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restriction_setting', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('n')->nullable();
            $table->enum('d', ['day', 'week', 'month'])->nullable()->default('day');
            $table->enum('g', ['individual', 'group'])->nullable()->default('group');
            $table->char('tz')->nullable()->default('UTC');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restriction_setting');
    }
}
