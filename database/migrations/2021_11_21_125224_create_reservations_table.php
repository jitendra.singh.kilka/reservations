<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->mediumInteger('id', true);
            $table->enum('type', ['individual', 'group']);
            $table->mediumInteger('user_id')->nullable();
            $table->mediumInteger('group_id')->nullable()->index('group_id');
            $table->integer('reservation_timestamp_utc')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
