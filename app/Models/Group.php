<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $table = 'groups';
	public $timestamps = false;

    protected $fillable = [
		'name'
	];

	public function group_users()
	{
		return $this->hasMany(GroupUser::class);
	}

	public function reservations()
	{
		return $this->hasMany(Reservation::class);
	}
}
