<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestrictionSetting extends Model
{
	protected $table = 'restriction_setting';
	public $incrementing = false;
	public $timestamps = false;

    const G_INDIVIDUAL = 'individual';
    const G_GROUP = 'group';

    const D_DAY = 'day';
    const D_WEEK = 'week';
    const D_MONTH = 'month';

    const TZ_UTC = 'UTC';
    const TZ_ASIA_CALCUTTA = 'Asia/Calcutta';
    const TZ_AMERICA_NEWYORK= 'America/NewYork';



	protected $casts = [
		'id' => 'int',
		'n' => 'int'
	];

	protected $fillable = [
		'n',
		'd',
		'g',
		'tz'
	];
}
