<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
	protected $table = 'group_users';
	public $timestamps = false;

	protected $casts = [
		'group_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'group_id',
		'user_id'
	];

	public function group()
	{
		return $this->belongsTo(Group::class);
	}
}
