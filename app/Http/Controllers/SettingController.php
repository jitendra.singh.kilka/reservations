<?php

namespace App\Http\Controllers;

use App\Models\RestrictionSetting;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SettingController extends Controller
{
    public function index()
    {
        $settings = RestrictionSetting::orderBy('tz', 'desc')->paginate(5);

        return view('settings.index', compact('settings'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public static function setting($time_zone)
    {
        return RestrictionSetting::where('tz', '=', $time_zone)->first();
    }


    public function edit(RestrictionSetting $setting)
    {
        return view('settings.edit', compact('setting'));
    }

    public function create(RestrictionSetting $setting)
    {
        return view('settings.create', compact('setting'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'n' => [
                'required',
                'integer',
                'gt:0'
            ],
            'd' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::D_DAY, RestrictionSetting::D_WEEK, RestrictionSetting::D_MONTH])
            ],
            'g' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::G_GROUP, RestrictionSetting::G_INDIVIDUAL])
            ],
            'tz' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::TZ_UTC, RestrictionSetting::TZ_ASIA_CALCUTTA, RestrictionSetting::TZ_AMERICA_NEWYORK]),
                function ($a, $v, $f) use ($request) {
                    if (RestrictionSetting::where([
                        'g' => $request->get('g'),
                        'tz' => $v
                    ])->exists()) {
                        $f("Setting Already exists.");
                    }
                }
            ]
        ]);
        RestrictionSetting::create($request->all());

        return redirect()->route('settings.index')
            ->with('success', 'Settings Created successfully');
    }

    public function update(Request $request, RestrictionSetting $setting)
    {
        $request->validate([
            'n' => [
                'required',
                'integer',
                'gt:0'
            ],
            'd' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::D_DAY, RestrictionSetting::D_WEEK, RestrictionSetting::D_MONTH])
            ],
            'g' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::G_GROUP, RestrictionSetting::G_INDIVIDUAL])
            ],
            'tz' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::TZ_UTC, RestrictionSetting::TZ_ASIA_CALCUTTA, RestrictionSetting::TZ_AMERICA_NEWYORK])
            ]
        ]);
        $setting->update($request->all());

        return redirect()->route('settings.index')
            ->with('success', 'Settings updated successfully');
    }
}
