<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupUser;
use App\Models\Reservation;
use App\Models\RestrictionSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ReservationController extends Controller
{
    public function index()
    {
        $user_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        return view('reservations.index', compact('user_ids'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'user_ids' => [
                'required',
                'array',
                'min:1'
            ],
            'user_ids.*' => [
                'required',
                'integer'
            ],
            'reservation_datetime' => [
                'required',
                'array',
                'min:1',
                'max:1'
            ],
            'reservation_datetime.*' => [
                'required',
                'date_format:d-m-Y H:i:s'
            ],
            'time_zone' => [
                'required',
                'string',
                Rule::in([RestrictionSetting::TZ_UTC, RestrictionSetting::TZ_ASIA_CALCUTTA, RestrictionSetting::TZ_AMERICA_NEWYORK])
            ]
        ]);
        $user_ids = $request->get('user_ids');
        $date_time = $request->get('reservation_datetime')[0];
        $setting = SettingController::setting($request->get('time_zone'));
        $restricted_user_ids = [];
        if ($setting) {
            $date_time_obj = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($date_time), $request->get('time_zone'))->timezone(RestrictionSetting::TZ_UTC);
            switch ($setting->g) {
                case RestrictionSetting::G_INDIVIDUAL:
                    foreach ($user_ids as $user_id) {
                        $query = Reservation::where([
                            'user_id' => $user_id
                        ]);
                        switch ($setting->d) {
                            case RestrictionSetting::D_DAY:
                                $start_time = $date_time_obj->startOfDay()->timestamp;
                                $end_time = $date_time_obj->endOfDay()->timestamp;
                                break;
                            case RestrictionSetting::D_WEEK:
                                $start_time = $date_time_obj->startOfWeek()->timestamp;
                                $end_time = $date_time_obj->endOfWeek()->timestamp;
                                break;
                            case RestrictionSetting::D_MONTH:
                                $start_time = $date_time_obj->startOfMonth()->timestamp;
                                $end_time = $date_time_obj->endOfMonth()->timestamp;
                                break;
                        }
                        $reservations = $query->whereBetween('reservation_timestamp_utc', [$start_time, $end_time])->count();
                        if ($reservations >= $setting->n) {
                            $restricted_user_ids[] = $user_id;
                        }else{
                            Reservation::create([
                                'reservation_timestamp_utc' => $date_time_obj->timestamp,
                                'type' => RestrictionSetting::G_INDIVIDUAL,
                                'user_id' => $user_id
                            ]);
                        }
                    }
                    break;
                case RestrictionSetting::G_GROUP:
                    sort($user_ids);
                    $group_name = implode(',', $user_ids);
                    $group = Group::where([
                        'name' => $group_name
                    ])->first();
                    if($group){
                        $query = Reservation::where([
                            'group_id' => $group->id
                        ]);
                        switch ($setting->d) {
                            case RestrictionSetting::D_DAY:
                                $start_time = $date_time_obj->startOfDay()->timestamp;
                                $end_time = $date_time_obj->endOfDay()->timestamp;
                                break;
                            case RestrictionSetting::D_WEEK:
                                $start_time = $date_time_obj->startOfWeek()->timestamp;
                                $end_time = $date_time_obj->endOfWeek()->timestamp;
                                break;
                            case RestrictionSetting::D_MONTH:
                                $start_time = $date_time_obj->startOfMonth()->timestamp;
                                $end_time = $date_time_obj->endOfMonth()->timestamp;
                                break;
                        }
                        $reservations = $query->whereBetween('reservation_timestamp_utc', [$start_time, $end_time])->count();
                        if ($reservations >= $setting->n) {
                            $restricted_user_ids = $user_ids;
                        }
                    }else{
                        $group = Group::create([
                            'name' => $group_name
                        ]);
                        foreach($user_ids as $user_id){
                            GroupUser::create([
                                'group_id'=>$group->id,
                                'user_id'=>$user_id
                            ]);
                        }
                    }
                    if(empty($restricted_user_ids)){
                        Reservation::create([
                            'reservation_timestamp_utc' => $date_time_obj->timestamp,
                            'type' => RestrictionSetting::G_GROUP,
                            'group_id' => $group->id
                        ]);
                    }
                    break;
            }
            return [
                'data' => [
                    'is_booking_restricted' => $restricted_user_ids ? true : false,
                    'restricted_user_ids' => $restricted_user_ids
                ]
            ];
        }else{
            $validator = Validator::make([], []);
            $validator->errors()->add('setting', 'timezone setting not valid.');
            throw new ValidationException($validator);
        }

    }
}
