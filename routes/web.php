<?php

use App\Http\Controllers\ReservationController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;


Route::resource('settings', SettingController::class);
Route::resource('reservations', ReservationController::class);
